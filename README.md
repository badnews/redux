"REDUX"
-------

#### A redis RPC system. ####


### TODO ###

* review timeouts
* add inline docs
* use lock-protected counter for "result key generation

### INSPIRED BY ###

- https://github.com/kmanley/zerorpc
- https://github.com/nfarring/redisrpc
- https://github.com/amol-/repc

### EXAMPLE ###

see `tests/test_srv.py` for server and `tests/test_cl.py` for client

