import traceback
import redis as REDIS
import json
from random import randint

# logging setup
import logging
fmt = '[%(levelname)s] %(asctime)s, (%(threadName)-10s) %(message)s'
datefmt = '%H:%M:%S' #:%s'

LOG = logging.getLogger(__name__)
LOG.setLevel(logging.DEBUG)
formatter = logging.Formatter(fmt)
handler = logging.StreamHandler()
handler.setFormatter(formatter)
LOG.addHandler(handler)

#
# WIRE FORMAT.
#
# request: [ token (str), func_name (str), args (list), kwargs (dict), ]
# response: [ success (bool), result (obj), error (error_dict), ]
# error_dict: { type (str:"exception"/"internal"),
#               message (str), traceback (str), }
#

class ReduxError(Exception):
    pass

class ReduxBase(object):
    def __init__(self, id, loglevel=None, cx=None, **kwargs):
        if loglevel:
            LOG.setLevel(loglevel)
        self.id = id
        self.cx = cx if cx else REDIS.StrictRedis(**kwargs)

    @property
    def key(self):
        return 'redux:%s' % self.id

    @property
    def cmd_key(self):
        return self.key + ':cmd'

class ReduxServer(ReduxBase):
    def __init__(self, id, obj, **kwargs):
        super(ReduxServer, self).__init__(id, **kwargs)
        self.obj = obj

    def _make_response(self, result=None, error=None):
        success = True if not error else False
        return [success, result, error]

    @staticmethod
    def _make_error(name, msg, err_type='exception', tb_str=''):
        return {
            'type': err_type,
            'name': name,
            'message': msg,
            'traceback': tb_str,
        }

    def _call(self, payload):
        token, func_name, args, kwargs = payload
        try:
            # c.f. http://bytes.com/topic/python/answers/830134-getattr-nested-attributes
            m = reduce(getattr, func_name.split("."), self.obj)
            result = m(*args, **kwargs)
            resp = self._make_response(result=result)
        except Exception, e:
            name = e.__class__.__name__
            if name in ['AttributeError']:
                error = ReduxServer._make_error(name, "Method not found",
                                                err_type='internal',
                                                tb_str=traceback.format_exc())
            else:
                error = ReduxServer._make_error(name, str(e),
                                                tb_str=traceback.format_exc())
            LOG.error("(FAIL:%s) %s: %s" % (func_name, error['name'],
                                            error['message']))
            resp = self._make_response(error=error)
        _resp = json.dumps(resp)
        LOG.debug("(->): %s" % _resp)
        self.cx.rpush(self.key+':'+token, _resp)

    def run(self):
        while True:
            key, item = self.cx.blpop([self.key, self.cmd_key])
            if key == self.cmd_key:
                # TODO: think more about command handling
                token, cmd = json.loads(item)
                LOG.debug("(<-CMD): %s" % cmd)
                if cmd == 'DIE':
                    break
                continue
            # normal execution path
            LOG.debug("(<-): %s" % item)
            payload = json.loads(item)
            self._call(payload)
        # ACK the DIE command
        LOG.debug("(DIE) terminated main loop")
        self.cx.rpush(self.cmd_key+':'+token, 'OK')


class ReduxClient(ReduxBase):
    def __init__(self, id, timeout=0, **kwargs):
        super(ReduxClient, self).__init__(id, **kwargs)
        self.timeout = timeout

    class _Method(object):
        """Ripped from kmanley/zerorpc which was ripped from xmlrpclib"""
        def __init__(self, send, name):
            self.__send = send
            self.__name = name

        def __getattr__(self, name):
            """Called recursively to build full (dotted) function call name"""
            return ReduxClient._Method(self.__send, "%s.%s" % (self.__name, name))

        def __call__(self, *args, **kwargs):
            return self.__send(self.__name, *args, **kwargs)

    def __getattr__(self, name):
        return ReduxClient._Method(self._call, name)

    def _call(self, func_name, *args, **kwargs):
        token = '%08x' % randint(0, 0xffffffffL)
        req = json.dumps([token, func_name, args, kwargs])
        LOG.debug("(->): %s" % req)
        self.cx.rpush(self.key, req)
        resp = self.cx.blpop(self.key+':'+token, timeout=self.timeout)
        if not resp:
            _msg = ("(TIMEOUT ERROR): no response after %d seconds\n"
                    "(..) call: %s, args: %s, kwargs: %s"
                    % (self.timeout, func_name, args, kwargs))
            LOG.error(_msg)
            raise ReduxError('timeout error (%s)' % func_name)
        q, item = resp
        LOG.debug("(<-): %s" % item)
        success, result, error = json.loads(item)
        if not success:
            _msg = ("(REMOTE ERROR): %s -- %s\n"
                    "(..) call: %s, args: %s, kwargs: %s"
                    % (error['name'], error['message'], func_name, args, kwargs))
            if error['type'] == 'exception':
                _msg += "\n(..) trace: %s" % error['traceback']
            LOG.error(_msg)
            raise ReduxError('%s: %s' % (error['name'], error['message']))
        else:
            return result

    # TODO: refactor and merge with _call code
    def _send_cmd(self, data):
        token = '%08x' % randint(0, 0xffffffffL)
        LOG.debug("(->CMD): %s" % data)
        req = json.dumps((token, data))
        self.cx.rpush(self.cmd_key, req)
        resp = self.cx.blpop(self.cmd_key+':'+token, timeout=self.timeout)
        if not resp:
            _msg = ("(TIMEOUT ERROR): no response after %d seconds\n"
                    "(..) CMD: %s" % (self.timeout, data))
            LOG.error(_msg)
            raise ReduxError('timeout error (CMD)')
        q, item = resp
        LOG.debug("(<-CMD): %s" % item)
        return item
