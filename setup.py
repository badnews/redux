#!/usr/bin/env python

from distutils.core import setup

setup(name='Redux',
      version='0.2',
      description='Redis RPC framework',
      author='Rav Chandra',
      author_email='rav@elysium.co.nz',
      packages = ['redux'],
      requires = ['redis'],
     )

