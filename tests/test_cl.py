from redux import ReduxClient
from calc import Calculator


def do_calculations(calculator):
    #calculator.foor.blah.nest.ed(5, zz=True)
    calculator.clr()
    #calculator.foo.nesedt()
    calculator.add(5)
    calculator.sub(3)
    calculator.mul(4)
    calculator.div(2)
    assert calculator.val() == 4
    #try:
    #    calculator.missing_method()
    #    assert False
    #except (AttributeError):#, redisrpc.RemoteException):
    #    pass


# 1. Local object
#calculator = Calculator()
#do_calculations(calculator)

# 2. Remote object, should act like local object
calculator = ReduxClient('id1')
#redis_server = redis.Redis()
#message_queue = 'calc'
#calculator = redisrpc.Client(redis_server, message_queue, timeout=1)
do_calculations(calculator)
print('success!')
